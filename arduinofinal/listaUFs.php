
<?php

    
	include 'conexao.php';
	
	$consulta = 'select id_uf, nome from uf';
	
	$resultado = $con->query($consulta);
	
    if(!$resultado) {
		printf("Erro na consulta: %s\n", $con->error);
		die();
	}
    echo "<select name='uf' onchange='buscaCidades(this.value)'>";
	echo "<option>----Selecione um estado	----</option>";
	while($linha = $resultado->fetch_array(MYSQLI_ASSOC)) {
    
		echo "<option value='".$linha['id_uf']."'>".$linha['nome']."</option>";
		
	}
	echo "</select>";

	include 'desconecta.php';
	
?>



