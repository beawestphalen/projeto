<html>
	<head>
		
		 <link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/menu.css" rel="stylesheet"></head>
<?php
    session_start();
	    if(!isset($_SESSION['usuario'])){
	       echo('
            <div class="row">
		    <div class="col-md-12">
			    <nav class="menu" role="navigation">
				    <div class="navbar-header marca" >
					     <a class="navbar-brand" href="index.php" ><img  class="brand" alt="Marca" src="brand.png"></a>
					    
					    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						     <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					    </button> 
				    </div>
				    
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					    <ul class="nav navbar-nav">
							    <li>
								    <a class="menu_nome" href="index.php#sobre">Sobre</a>
							    </li>
							    <li>
								    <a class="menu_nome" href="index.php#noticias">Notícias</a>
							    </li>
							    <li>
								    <a class="menu_nome"  href="cadastro.php">Cadastre-se</a>
							    </li>
					    </ul>
					    
			    <ul class="nav navbar-nav navbar-right">
				    
				<li class="dropdown">
				    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Conecte-se<strong class="caret"></strong></a>
				    <ul class="dropdown-menu">
					<div class="form-group">		 <form action="processarLogin.php" method="POST">
										    <label for="inputEmail3" class="col-sm-2 control-label">
										    <p class="cor">Usuário </p>
										    </label>
									    <div class="col-sm-10">
										    <input type="Usuario" name="login" class="form-control" id="login" />
									    </div>
									</div>
									    <div class="form-group">					
										    <label for="inputPassword3" class="col-sm-2 control-label">
										    <p class="cor">Senha</p>
										    </label>
									    <div class="col-sm-10">
										    <input type="password" name="senha" class="form-control" id="senha" />
									    </div>
									</div>
									    <div class="form-group">
										    <div class="col-sm-offset-2 col-sm-10">
																     
										    <label>
										    <a id="modal-905037" href="#modal-container-905037" role="button" class="cor" data-toggle="modal">Problemas?</a>
						
										    </label>					
											    
										    </div>
									    <div class="form-group">
										    <div class="col-sm-offset-2 col-sm-10">					 
											    <button type="submit" class="btn btn-sucess"><b>
											    Entrar
												    <span class="glyphicon glyphicon-log-in" aria-hidden="true"></span>
											    </button></b>
											</form>
										    </div>
					</div>
				    </ul>
				</li>
			    </ul>
			</div>
		    </nav>
		</div>
	   </div>
	    ');
	    }else if($_SESSION['usuario']=='admin'){
		    echo('
			    <div class="container-fluid">
		<div class="row">
		    <div class="col-md-12">
			<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			    <div class="navbar-header marca">
				<a class="navbar-brand" href="index.php"><img class="brand" alt="Marca" src="brand.png"></a>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				    <span class="sr-only">Toggle navigation</span>
				    <span class="icon-bar"></span>
				    <span class="icon-bar"></span>
				    <span class="icon-bar"></span>
				</button>
			    </div>
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
				    <li>
					<a href="index.php#sobre">Sobre</a>
				    </li>
				    <li>
					<a href="index.php#noticias">Notícias</a>
				    </li>
                    <li>
					<a href="administrador.php">Administração</a>
				    </li>                   
				</ul>
				<ul class="nav navbar-nav navbar-right">
                		<li>		    
                            <a href="sair.php">Sair <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span></a>
					    </li>
					</ul>
				    </li>
				</ul>
			    </div>
			</nav>
		    </div>
		</div>
	    </div>
		    ');
	    }else{
		    echo('
            <div class="row">
		    <div class="col-md-12">
			<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			    <div class="navbar-header marca">
				<a class="navbar-brand" href="index.php"><img class="brand" alt="Marca" src="brand.png"></a>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				    <span class="sr-only">Toggle navigation</span>
				    <span class="icon-bar"></span>
				    <span class="icon-bar"></span>
				    <span class="icon-bar"></span>
				</button>
			    </div>
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
				    <li>
					<a href="index.php#sobre">Sobre</a>
				    </li>
				    <li>
					<a href="index.php#noticias">Notícias</a>
				    </li>
				    <li>
					<a href="duvidas.php">Dúvidas</a>
				    </li>
                    <li>
					<a href="respostas.php">Respostas</a>
				    </li>
                     <li>
                    <a href="principal.php">Principal</a>
				    </li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
				    <li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Perfil <strong class="caret"></strong></a>
					<ul class="dropdown-menu">
					    <li>
						<a href="perfil.php"><b>Editar </b><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
					    </li>
					    <li>
						<a href="sair.php"><b>Sair </b><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span></a>
					    </li>
					</ul>
				    </li>
				</ul>
			    </div>
			</nav>
		    </div>
		</div>
		    ');
	    }
?>
</html>
