<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Arduíno</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/relatorios.css" rel="stylesheet">

  </head>
  <body>   
            <?php 
            require ('conexao.php');
            require('menu.php');
            $id = $_SESSION['id'];
            $tpdeslizamento = 2;
            ?>   
               <div class="container-fluid">
                <div class="row">
                    <div class="topo"></div>
                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                        <div class="btn-group" role="group">
                            <a href="sensores.php" class="btn btn-default ">Sensores</a>
                        </div>
                        <div class="btn-group" role="group">
                            <a href="relatorios.php" class="btn btn-default btn-relatorios">Relatórios</a>
                        </div>
                        <div class="btn-group" role="group">
                            <a href="parametros.php" class="btn btn-default">Parâmetros</a>
                        </div>
                    </div>
                </div>
                
                <?php 
                #ler entradas arduino dos sensores registrados para localização do sensor e umidade do usuario 
                //$query = "SELECT id_monitoramento, umidade FROM monitoramento, umidade WHERE Usuario_id_usuario = $id AND sensor_id_sensor = id_monitoramento";
               
               $query= "SELECT monitoramento.id_monitoramento, umidade.umidade as um, parametrosuser.id_declividade as idc, parametrosuser.Tiposolo_id_tiposolo as solo, umidade.data as dataum, umidade.hora as horaum, tiposolo.nome as nmsolo, declividade.grau as grau
               FROM monitoramento, umidade, parametrosgerais, parametrosuser, tiposolo, declividade
WHERE monitoramento.Usuario_id_usuario = $id
  AND umidade.sensor_id_sensor = monitoramento.id_monitoramento 
  AND parametrosuser.Usuario_id_usuario = monitoramento.Usuario_id_usuario
  AND parametrosgerais.declividade_id_declividade = parametrosuser.id_declividade 
  AND parametrosgerais.tipo_id_tiposolo = parametrosuser.Tiposolo_id_tiposolo
  AND parametrosgerais.deslizamento_id_deslizamento = $tpdeslizamento 
  AND umidade.umidade >= parametrosgerais.umidade_final
  AND tiposolo.id_tiposolo = parametrosuser.Tiposolo_id_tiposolo
  AND declividade.id_declividade = parametrosuser.id_declividade ";
		
                $resultado = $con->query($query);
                //var_dump($resultado);

           echo "<div class = 'row'><div class = 'col-md-2'></div><div class='col-md-8'>";
           echo "<div class = 'jumbotron'><div class='table-responsive'> <table class='table' border='0' style='margin-left:0%;'>";
           echo "<thead style='color:#1A8F98; text-align: center;'>  <tr><td class='margem'><b> Data</b></td><td class='margem'><b>Hora</b></td><td class='margem'><b>Declividade</b></td><td class='margem'><b>Tipo do Solo</b></td><td class='margem'><b>Umidade</b></td></tr></thead>";
           echo "<tbody style='color:rgb(0, 151, 156); text-align: center;'>";
                
                if($resultado){
                  while($linha = $resultado->fetch_array(MYSQLI_ASSOC)){
                    echo "<tr><td class='margem'>".$linha['dataum']."</td><td class='margem'>".$linha['horaum']."</td><td class='margem'>".$linha['grau']."</td><td class='margem'>".$linha['nmsolo']."</td><td class='margem'>".$linha['um']."</td></tr></div>";
                  }
                }else{
                        echo"<div class = 'row'>Não há relatório efetuado.</div>";
                }
                 echo "</div><div class='col-md-2'></div></div>";
                ?>
            
                <script src="js/jquery.min.js"></script>
                <script src="js/bootstrap.min.js"></script>
                <script src="js/scripts.js"></script></div></div>
                <div class="section">

</body>
</html>

