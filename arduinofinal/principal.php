<html><head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Arduíno</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/principal.css" rel="stylesheet">
        

    </head>
<body>
   <?php require('menu.php'); ?>
   
               <div class="container-fluid" style="padding: 0px;">
                <div class="row">
                    <div class="topo"></div>
                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                        <div class="btn-group" role="group">
                            <a href="sensores.php" class="btn btn-default btn-sensores">Sensores</a>
                        </div>
                        <div class="btn-group" role="group">
                            <a href="relatorios.php" class="btn btn-default">Relatórios</a>
                        </div>
                        <div class="btn-group" role="group">
                            <a href="parametros.php" class="btn btn-default">Parâmetros</a>
                        </div>
                    </div>
                </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="jumbotron">
                            <div class="row">
                                <h1 class="bemvindo text-center">BEM-VINDO!</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="row">
                <div class="form-group">                    
                    <div class="col-md-12">
                        <div class="jumbotron jumboblue">
                            <div class="row">
                                <div class="col-md-4">
                                    <h1>PASSO 1</h1>
                                </div>
                         <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <h3>Sensores: Local onde ficarão cadastrados os sensores</h3>
                                    <p><h3>Criação: Criar o sensor</h3></p>
                                    <p><h3>Monitoramento: Local onde é monitorado o solo através do sensor</h3></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
       
           <div class="row">
                <div class="form-group">                    
                    <div class="col-md-12">
                        <div class="jumbotron margintop">
                            <div class="row">
                                <div class="col-md-4">
                                    <h1>PASSO 2</h1>
                                </div>
                            <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <h3>Parâmetros: São as características do local onde será realizado o monitoramento 
                                                (tipo do solo, declividade e intensidade da chuva)
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                 <div class="row">
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="jumbotron jumboblue margintop">
                            <div class="row">
                                <div class="col-md-4">
                                    <h1>PASSO 3</h1>
                                </div>
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <h3>Relatórios: São os dados gerados através da relação entre 
                                        o monitramento e os paramêtros</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
         <div class="row">
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="jumbotron margintop">
                            <div class="row">
                                <div class="col-md-4">
                                    <h1>PASSO 4</h1>
                                </div>
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <h3>Dúvidas: Local para enviar dúvidas caso os passos não esclareçam</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        
          <div class="row">
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="jumbotron jumboblue margintop">
                            <div class="row">
                                <div class="col-md-4">
                                    <h1>PASSO 5</h1>
                                </div>
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <h3>Baixar programa, código e manual do arduíno:</h3>                                    
                                    <a href="baixar.php?arquivo=arquivos/Arduino.zip"><h3 style="color:black;">cod_arduino.zip</h3></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php require('footer.php'); ?>


        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/scripts.js"></script>
</body>
</html>
