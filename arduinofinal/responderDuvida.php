<html>
<head>
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Arduíno</title>
        <meta name="description" content="Source code generated using layoutit.com">
        <meta name="author" content="LayoutIt!">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/problemas.css" rel="stylesheet">
        <link href="css/principal.css" rel="stylesheet">


        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/scripts.js"></script>
    
</head>

<body>
<?php 
	require('conexao.php');
    require('menu.php');
?>
<div class="container-fluid">
             <div class="row">
                <div class="form-group">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="jumbotron">
                            <div class="row">
                                <div class="col-md-12">
                                    <form method='POST' action='processarResposta.php'>
                                        <?php

                                            $id = $_GET["id"];
                                            $resultado = mysqli_query($con, "SELECT * FROM questao WHERE id_questao=$id and status = 0");

                                            while($consulta = mysqli_fetch_array($resultado)) {
                                                    echo "<h4>Dúvida: ".$consulta["questao"]." <input type='hidden' name='id' value='".$id."'/></h4>";
                                                }
                                        ?>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="exampleInputResposta"><h4>Resposta: </h4></label>
                                    <textarea class="form-control" id="exampleInputText" rows="5" name="resposta" required/></textarea>
                                </div>
                            </div>
                            <br>
                             <div class="row">
                                 <div class="col-md-10">
                                    <button type="submit" class="btn btn-responde"/><b>Responder <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></b></button>
                                 </div>
                             </div>
 
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                                    </form>
                </div>
            </div>
        </div>
</body>
