<?php

    
	include 'conexao.php';
	
	$consulta = 'select id_declividade, grau from declividade';
	
	$resultado = $con->query($consulta);
	
    if(!$resultado) {
		printf("Erro na consulta: %s\n", $con->error);
		die();
	}
    echo "<select name='grau' onchange='buscaDeclividade(this.value)'>";
	echo "<option>----Selecione um valor para declividade----</option>";
	while($linha = $resultado->fetch_array(MYSQLI_ASSOC)) {
    
		echo "<option value='".$linha['id_declividade']."'>".$linha['grau']."</option>";
		
	}
	echo "</select>";

	include 'desconecta.php';
	
?>

